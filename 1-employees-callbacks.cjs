/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

let employeeDetails = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const fs = require("fs");
const path = require('path');

function fileOperation() {
    fs.readFile(path.join(__dirname, "./data.json"), "utf-8", (err, data) => {   //reading data file
        if (err) {
            console.error(err);
        }
        else {
            let myData = JSON.parse(data);
            let myValue = Object.values(myData);
            let employeeDetail = (myValue[0]);
            let output = employeeDetail.filter(employee => {
                if ((employee.id == 23) || (employee.id == 13) || (employee.id == 2)) {       //1. Retrieve data for ids : [2, 13, 23].
                    return employee
                }

            })
            fs.writeFile(path.join(__dirname, "retrived_data.json"), JSON.stringify(output), (err) => {
                if (err) {
                    console.error(err);
                }

                else {
                    let myGroup = employeeDetail.reduce((accumulator, startingvalue) => {
                        if (accumulator.hasOwnProperty(startingvalue.company)) {
                            accumulator[startingvalue.company].push(startingvalue);
                        }
                        else {
                            accumulator[startingvalue.company] = [];
                            accumulator[startingvalue.company].push(startingvalue);         //  2. Group data based on companies.
                        }
                        return accumulator
                    }, {})
                    fs.writeFile(path.join(__dirname, "grouped_data.json"), JSON.stringify(myGroup), (err) => {
                        if (err) {
                            console.error(err);
                        }
                        else {

                            let PowerpuffBrigadeDetails = employeeDetail.filter((employee) => {        //   3. Get all data for company Powerpuff Brigade
                                if (employee.company === "Powerpuff Brigade") {
                                    return employee
                                }
                            })
                            fs.writeFile(path.join(__dirname, "powerpuffbrigadedetails.json"), JSON.stringify(PowerpuffBrigadeDetails), (err) => {
                                if (err) {
                                    console.error(err);
                                }
                                else {

                                    let deleteId = employeeDetail.filter((employee) => {     //   4. Remove entry with id 2.
                                        if (employee.id !== 2) {
                                            return employee
                                        }

                                    })
                                    fs.writeFile(path.join(__dirname, "deletedid.json"), JSON.stringify(deleteId), (err) => {
                                        if (err) {
                                            console.error(err);
                                        }
                                    })
                                    let sortComapanies = employeeDetail.sort((employee1, employee2) => {    // 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
                                        if (employee1.company === employee2.company) {
                                            if (employee1.id < employee2.id) {
                                                return -1
                                            } else {
                                                return 1
                                            }
                                        } else {
                                            if (employee1.company < employee2.company) {
                                                return -1
                                            } else {
                                                return 1
                                            }
                                        }

                                    })
                                    fs.writeFile(path.join(__dirname, "sortedOutput.json"), JSON.stringify(sortComapanies), (err) => {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            let birthAdd = employeeDetail.map((employee) => {
                                                if (employee.id % 2 == 0) {
                                                    employee.birthday = new Date();
                                                }
                                                return employee
                                            })
                                            fs.writeFile(path.join(__dirname, "birtdayadded.json"), JSON.stringify(birthAdd), (err) => {
                                                if (err) {
                                                    console.error(err);
                                                }
                                            })

                                        }
                                    })
                                }
                            })

                        }
                    })

                }
            })








        }
    })
}
fileOperation()

